#!/usr/bin/env python3

import pandas as pd
import os
from optparse import OptionParser


def main():
    parser = OptionParser()
    parser.add_option("-i", "--index", dest="input", action='store', help="barcodes index for samples")
    parser.add_option("-o", "--output", dest='output', action="store", help="directory where to save samples data")
    (options, args) = parser.parse_args()

    print(options.input, options.output)

    if options.output is None or options.input is None and len(args) == 0:
        print('Fill input/output')
        return -1

    file_list = args
    df = pd.read_csv(options.input)

    for i, row in df.iterrows():
        print(row['Sample'])
        # Combination of demuxed fastas that can be for a sample
        # There can only be 2 or 1 files per sample from this
        file_1 = row['Rv 1'] + '_' + row['Fw 1'] + '.fasta'
        file_2 = row['Rv 2'] + '_' + row['Fw 2'] + '.fasta'
        file_3 = row['Fw 1'] + '_' + row['Rv 1'] + '.fasta'
        file_4 = row['Fw 2'] + '_' + row['Rv 2'] + '.fasta'

        fasta_combination = [file_1, file_2, file_3, file_4]
        sample_files = [demux_fasta for demux_fasta in file_list
                        if any(fasta in demux_fasta for fasta in fasta_combination)]

        # Concat sample_files
        if len(sample_files) > 0:
            with open(os.path.join(options.output, 'sample_' + str(row['Sample']) + '.fasta'), 'w') as fasta:
                for fname in sample_files:
                    with open(fname) as infile:
                        for line in infile:
                            fasta.write(line)


if __name__ == '__main__':
    main()
