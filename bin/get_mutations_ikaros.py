#!/usr/bin/env python3

import os
import pandas as pd
from glob import glob
from cyvcf2 import VCF, Writer
from optparse import OptionParser


# REF GEN:
# IKZF1 IKAROS family zinc finger 1  [ Homo sapiens (human) ]
# NC_000007.14:50303453-50405101 Homo sapiens chromosome 7, GRCh38.p13 Primary Assembly
IKAROS_MUTATIONS = {}


def main():
    parser = OptionParser()
    #parser.add_option("-i", "--input_dir", dest="input", action='store', help="dir with vcfs")
    parser.add_option("-o", "--output", dest='output', action="store", help="filepath for output data")
    (options, args) = parser.parse_args()

    print(args, options.output)

    if options.output is None or len(args) == 0:
        print('Fill input/output')

    # pos + 50303453 - 1
    data = []
    header = ('sample', 'NC_000007.14:g.50402906T>G', 'AF.T>G', 'NC_000007.14:g.50403915G>A', 'AF.G>A')

    print('Getting mutations of interest...')
    for fn in args:
        vcf = VCF(fn)
        sample = os.path.basename(fn).split('.')[0].split('_')[-1]
        mut1 = False
        af1 = 'Na'
        mut2 = False
        af2 = 'Na'

        for variant in vcf:
            # From ref fasta, mutations happen in POS 99.454 and 100.463
            if variant.POS == 99454 and 'G' in variant.ALT:
                mut1 = True
                # af1 = variant.INFO.get('AF1')
                af1 = variant.format('FREQ')[0]
            if variant.POS == 100463 and 'A' in variant.ALT:
                mut2 = True
                # af2 = variant.INFO.get('AF1')
                af2 = variant.format('FREQ')[0]

        data.append([sample, mut1, af1, mut2, af2])
        vcf.close()

    df = pd.DataFrame(data, columns=header)
    df.to_csv(options.output, index=False)
    print('Done!')


if __name__ == '__main__':
    main()
