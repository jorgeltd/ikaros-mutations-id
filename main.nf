fastq_file = file(params.sequences)
bc_3p = file(params.bc_3p)
bc_5p = file(params.bc_5p)
sample_index = file(params.sample_index)
gen_ref = file(params.reference)


process quality_filter{
    input:
    file fastq_file

    output:
    file "reads_q${params.seqkit.Q}.fastq" into quality_reads_ch

    script:
    """
    seqkit seq $fastq_file -Q ${params.seqkit.Q} -m ${params.seqkit.min_len} -j 8  > reads_q${params.seqkit.Q}.fastq
    """

}

process demux_3p{
    publishDir "${params.output}/3p", mode: "copy"

    input:
    file reads from quality_reads_ch

    output:
    file "*.fasta" into demuxed_3p_ch

    script:
    """
    cutadapt $reads -e ${params.cutadapt.error} -a file:${bc_3p} --no-indels --quiet --discard-untrimmed -o {name}.fasta 
    """
}

process demux_5p{
    input:
    each file(demuxed_3p_fasta) from demuxed_3p_ch

    output:
    file "*_*.fasta" into demuxed_5p_ch
    
    script:
    base = "${demuxed_3p_fasta.baseName}"
    """
    cutadapt $demuxed_3p_fasta -e ${params.cutadapt.error} -g file:${bc_5p} --no-indels --quiet --discard-untrimmed -o ${base}_{name}.fasta 
    """
}


process get_samples{
    publishDir "${params.output}/demux", mode: "copy"

    input: 
    file demux_reads from demuxed_5p_ch.collect() 

    output:
    file "sample_*.fasta" into samples_ch

    script:
    """
    get_samples.py $demux_reads -i ${sample_index} -o \$PWD
    """

}

process align_sample_reads{
    publishDir "${params.output}/sam", mode: "copy"

    input: 
    each file(sample_reads) from samples_ch

    output:
    file "${sample_reads.simpleName}.sam" into sam_ch

    script:
    base = "${sample_reads.baseName}"
    """
    bwa mem -x ont2d -t 2 ${gen_ref} $sample_reads > "${sample_reads.baseName}.sam"
    """

}

process sort_sam{
    publishDir "${params.output}/bam", mode: "copy"
    input:
    each file(sam_file) from sam_ch

    output:
    file "${sam_file.simpleName}.bam" into bam_ch

    script:
    """
    samtools view -u $sam_file | samtools sort -o ${sam_file.simpleName}.bam
    """

}

process mpileup_bam{
    publishDir "${params.output}/mpileup", mode: "copy"
    input:
    each file(bam_file) from bam_ch

    output:
    file "${bam_file.simpleName}.mpileup" into mpileup_ch

    script:
    """
    samtools mpileup -f ${gen_ref} -B $bam_file > ${bam_file.simpleName}.mpileup
    """
}

process varscan_snp{
    publishDir "${params.output}/vcf", mode: "copy"
    
    input:
    each file(mpileup_file) from mpileup_ch

    output:
    file "${mpileup_file.simpleName}.vcf.gz" into vcf_ch

    script:
    """
    varscan mpileup2snp $mpileup_file --output-vcf 1 --p-value ${params.varscan.pval} --min-avg-qual ${params.varscan.avg_q} --min-coverage ${params.varscan.min_cov} --strand-filter ${params.varscan.str_filter} | bgzip > ${mpileup_file.simpleName}.vcf.gz
    """
}

process filter_vcf{
    publishDir "${params.output}/vcf_filter", mode: "copy"

    input:
    each file(vcf_file) from vcf_ch

    output:
    file "${vcf_file.simpleName}.filtered.vcf.gz" into filtered_vcf_ch_1, filtered_vcf_ch_2

    script:
    """
    bcftools view $vcf_file -i 'ADP>${params.bcftools.ADP}' -Oz -o ${vcf_file.simpleName}.filtered.vcf.gz  
    """
}

process concensus_sample_sequence{
    publishDir "${params.output}/concensus", mode: "copy"

    input:
    each file(vcf_filtered) from filtered_vcf_ch_1

    output:
    file "${vcf_filtered.simpleName}.nf.fasta" into sample_concensus

    script:
    """
    bcftools index $vcf_filtered && bcftools consensus -f ${gen_ref} $vcf_filtered > ${vcf_filtered.simpleName}.nf.fasta
    """
}

process get_mutations{
    publishDir "${params.output}/mutations", mode: "copy"

    input:
    file vcf_filtered from filtered_vcf_ch_2.collect()

    output:
    file "mutations_data.csv" into mutations_ch

    script:
    """
    get_mutations_ikaros.py $vcf_filtered -o "mutations_data.csv"
    """
}










