#!/bin/bash

seq='dataset/LLC2/sequences/sequences.fastq.gz'
seq_q10='dataset/LLC2/sequences/filter_reads_q10.fastq'
fw_primer='dataset/LLC2/barcodes/ikaros_fixed_5p.fasta'
rv_primer='dataset/LLC2/barcodes/ikaros_fixed_3p.fasta'
fw_index='dataset/LLC2/barcodes/ikaros_index_5p.fasta'
rv_index='dataset/LLC2/barcodes/ikaros_index_3p.fasta'
ref='dataset/ikaros_ref/ikaros_ref.fasta'
out='demux_test'


mkdir -p $out $out/fw_primer $out/demux $out/rv_primer
mkdir -p $out/sam $out/bam $out/mpileup $out/vcf $out/cons

#demux by fordwar-dprimer
#keep trimmed seq --action=none
#discard not trimmed --discard-untrimmed 
#--untrimmed-output FILE
#-g 5'
#-a 3'

#con calidad 
cutadapt -e 0.35 --no-indels -g file:$fw_primer $seq_q10 -o $out/fw_primer/{name}.fasta

#sin calidad 
#cutadapt -e 0.35 --no-indels -g file:$fw_primer $seq -o $out/fw_primer/{name}.fasta

cutadapt -e 0.05 --no-indels -g file:$fw_index $out/fw_primer/primer_fw.fasta -o $out/fw_primer/{name}.fasta

for f in $out/fw_primer/Mut*.fasta
do
    name=$(basename "$f" .fasta)
    echo $name
    cutadapt -e 0.35 --no-indels -a file:$rv_primer $f -o $out/rv_primer/{name}.fasta
    cutadapt -e 0.05 --no-indels -a file:$rv_index $out/rv_primer/primer_rv.fasta -o $out/rv_primer/${name}_fw_{name}.fasta
done


mkdir -p $out/fw_primer/filter $out/rv_primer/filter
bwa mem -x ont2d -t 4 $ref $out/fw_primer/Mut1.fasta > $out/fw_primer/filter/Mut1.sam
bwa mem -x ont2d -t 4 $ref $out/fw_primer/Mut2.fasta > $out/fw_primer/filter/Mut2.sam


bwa mem -x ont2d -t 4 $ref $out/rv_primer/Mut1_fw_Mut1.fasta > $out/rv_primer/filter/Mut1.sam
bwa mem -x ont2d -t 4 $ref $out/rv_primer/Mut2_fw_Mut2.fasta > $out/rv_primer/filter/Mut2.sam


reference='dataset/ikaros_ref/ikaros_ref.fasta'

bwa index $reference

#para doble demultiplexado
#for f in $(ls | egrep '^.*([0-9]+).*\1.*.fasta')
data=$out/rv_primer
for f in $(ls $data | egrep '^.*([0-9]+).*\1.*.fasta')
do
  name=$(basename "$f" .fastq)
  bwa mem -x ont2d -t 2 $reference "$data/$f" > "$out/sam/$name.sam"
  samtools view -u "$out/sam/$name.sam" | samtools sort -o "$out/bam/$name.bam"
  bcftools mpileup -f $reference -B "$out/bam/$name.bam" > "$out/mpileup/$name.mpileup"
  bcftools call -cvOz -o "$out/vcf/$name.vcf.gz" "$out/mpileup/$name.mpileup"
  bcftools view -i 'QUAL>40 && DP>200' -Oz -o "$out/vcf/$name.filtered.vcf.gz" "$out/vcf/$name.vcf.gz"
  bcftools index "$out/vcf/$name.filtered.vcf.gz"
  bcftools consensus -f $reference "$out/vcf/$name.filtered.vcf.gz" > "$out/cons/$name.fa"
done







