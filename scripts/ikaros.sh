#!/usr/bin/env bash
 
barcode5p='dataset/LLC2/barcodes/bc_ikaros_5p.fasta'
seqs='dataset/LLC2/sequences/sequences.fastq.gz'
output='out_ikaros'

mut1_3p="TCCAGTACAGCTTTAGGTCTTCCTGTCTCTTATACACATCTCCGAGCCCACGAGAC"
mut2_3p="TCTAAACTGCTTTGCCCTTTCCCTGTCTCTTATACACATCTCCGAGCCCACGAGAC"

work='work_ikaros'

cd /home/jorge/Bioinformatics/nano-ighv

#calidad
echo "calidad"
mkdir -p $work/filter
filter_reads=$work/filter/filter_reads.fastq
#seqkit seq -Q 10 $seqs > $filter_reads


echo "demux"
mkdir -p $output/demux $work/demux


#demux forward
#keep trimmed seq --action=none
#discard not trimmed --discard-untrimmed 
#--untrimmed-output FILE
error=0.22
cutadapt -e $error -g  file:$barcode5p --no-indels --quiet -o $work/demux/{name}.fastq $filter_reads

#demux reverse
for f in $work/demux/Mut*
do
  name=$(basename "$f")
  echo $name
    if [[ $name == *"1"* ]]; then
        cutadapt -e $error -a $mut1_3p -j 4 --no-indels --discard-untrimmed  $f -o $output/demux/$name
    elif [[ $name == *"2"* ]]; then  
        cutadapt -e $error -a $mut2_3p -j 4 --no-indels --discard-untrimmed  $f -o $output/demux/$name
    fi
done

#cutadapt -e $error -a $mut1_3p -j 4 --no-indels --discard-untrimmed $work/demux/Mut1.FW.fastq -o $output/fw/MUT1.fastq 
#cutadapt -e $error -a $mut2_3p -j 4 --no-indels --discard-untrimmed $work/demux/Mut2.FW.fastq -o $output/fw/MUT2.fastq
#rm $output/*.FW.fastq


reference='dataset/ikaros_ref/ikaros_ref.fasta'
mkdir -p $output/sam $output/bam $output/mpileup $output/vcf $output/cons

bwa index $reference

#para doble demultiplexado
#for f in $(ls | egrep '^.*([0-9]+).*\1.*.fasta')
for f in $output/demux/*
do
  name=$(basename "$f" .fastq)
  bwa mem -x ont2d -t 2 $reference "$f" > "$output/sam/$name.sam"
  samtools view -u "$output/sam/$name.sam" | samtools sort -o "$output/bam/$name.bam"
  bcftools mpileup -f $reference -B "$output/bam/$name.bam" > "$output/mpileup/$name.mpileup"
  bcftools call -cvOz -o "$output/vcf/$name.vcf.gz" "$output/mpileup/$name.mpileup"
  bcftools view -i 'QUAL>35 && DP>200' -Oz -o "$output/vcf/$name.filtered.vcf.gz" "$output/vcf/$name.vcf.gz"
  bcftools index "$output/vcf/$name.filtered.vcf.gz"
  bcftools consensus -f $reference "$output/vcf/$name.filtered.vcf.gz" > "$output/cons/$name.fa"
done












