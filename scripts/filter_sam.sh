#/usr/bin/env bash

out='regions'
mkdir -p $out

for f in ./Mut*.sam 
do
    name=$(basename "$f" .sam)
    samtools view -u $f | samtools sort -o $out/$name.bam 
    samtools index $out/$name.bam
    samtools view $out/$name.bam "NC_000007.14:50303453-50405101:99000-99700" > $out/$name.r1.bam
    samtools view $out/$name.bam "NC_000007.14:50303453-50405101:100300-100800" > $out/$name.r2.bam
done
    
