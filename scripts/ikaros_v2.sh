#!/usr/bin/env bash
 
seqs='dataset/ikaros_samples/sequences.fastq.gz'
output='ikaros_results'

rv_barcode='dataset/ikaros_samples/bc_3p.fasta'
fw_barcode='dataset/ikaros_samples/bc_5p.fasta'
sample_index='dataset/ikaros_samples/barcode_index.csv'
reference='dataset/ikaros_ref/ikaros_ref.fasta'

cd /home/jorge/Bioinformatics/nano-ighv

#calidad
echo "calidad"
mkdir -p $output $output/filter
filter_reads=$output/filter/filter_reads.fastq
q=11
len=50
seqkit seq -Q $q -m $len $seqs > $filter_reads

echo "demux"
demux_rv=$output/demux/rv
demux_fw=$output/demux/fw
demux_samples=$output/demux/samples
mkdir -p $output/demux $output/demux $demux_rv $demux_fw $demux_samples

#demux forward
#keep trimmed seq --action=none
#discard not trimmed --discard-untrimmed 
#--untrimmed-output FILE
#-g 5p
#-a 3p

error=0.20
cutadapt -e $error -a file:$rv_barcode --no-indels --quiet --discard-untrimmed -o $demux_rv/{name}.fasta $filter_reads

for f in $demux_rv/*
do
    base=$(basename "$f" .fasta)
    echo $base
    cutadapt -e $error -g file:$fw_barcode --no-indels --quiet --discard-untrimmed $f -o $demux_fw/${base}_{name}.fasta 
done

python3 bin/get_samples.py -i $sample_index -d $demux_fw -o $demux_samples

mkdir -p $output/sam $output/bam $output/mpileup $output/vcf $output/cons

bwa index $reference

T=4
Q=35
D=150
adp=1500

pval=0.05
min_cov=100
avg_q=15
str_filter=1


for f in $demux_samples/*
do
  name=$(basename "$f" .fasta)  
  bwa mem -x ont2d -t $T $reference "$f" > "$output/sam/$name.sam"
  samtools view -u "$output/sam/$name.sam" | samtools sort -o "$output/bam/$name.bam"
  samtools mpileup -f $reference -B $output/bam/$name.bam > $output/mpileup/$name.mpileup
  varscan mpileup2snp $output/mpileup/$name.mpileup --output-vcf 1 --p-value $pval --min-avg-qual $avg_q --min-coverage $min_cov --strand-filter $str_filter > $output/vcf/$name.vcf      
  bgzip $output/vcf/$name.vcf
  
  #filter
  bcftools view -i 'ADP>1500' -Oz -o $output/vcf/$name.filtered.vcf.gz $output/vcf/$name.vcf.gz
  bcftools index "$output/vcf/$name.filtered.vcf.gz"
  bcftools consensus -f $reference "$output/vcf/$name.filtered.vcf.gz" > "$output/cons/$name.fa"
done

mkdir -p $output/mutations
python3 bin/get_mutations_ikaros.py -i $output/vcf -o $output/mutations/data.csv








