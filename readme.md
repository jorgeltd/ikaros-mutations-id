# Ikaros Mutations Identification
Pipeline in [nextflow](https://www.nextflow.io) to identify if two snv in the ikaros gen exist.

## How to run it
- Modify `nextflow.config` to your needs
- `nextflow run main.nf` to use a conda environment

## Dependencies
- Python >= 3.7.0, <3.8.0
- Python packages: pandas, cyvcf2
- Seqkit
- Bcftools
- Bwa
- Cutadapt
- Varscan2
- Samtools
- Nextflow

With conda, `environment.yml` can be used to create a environment with all the dependencies.